apply() {
    repo_url=$1
    echo $repo_url
    branch_name=feature/DATOOLS-851-migrate-airflow
    cd k8s-infra/
    git remote rm origin
    git remote add origin $repo_url
    git fetch
    git checkout main
    git reset --hard origin/main
    git switch -C main origin/main
    git push origin --delete $branch_name
    git branch -D $branch_name
    git checkout -b $branch_name
    # git reset --hard origin/$branch_name

    # yq e -i '.team = $name' terraform/common.yaml

    # git pull
    # git switch -C $branch_name origin/$branch_name
    git stash apply stash^{/"On main: try23"}
    git status
    git add .
    git commit -am "NOTICKET: add new next airflow instance in parallel to existing instance"
    git push -u origin $branch_name --force -o merge_request.create -o merge_request.target="main"
    cd ..
}

# for name in "analyticsb2b" "brandaid" "cdi" "code2success" "kassandra" "knowledge-graph" "kolumbus" "onair-interdependency-analysis" "onair-reach-prediction"	"panelytics" "product42" "signaturecode" "vipa"	 
# do
# apply "git@gitlab.com:dataalliance/products/$name/infra/k8s-infra.git"
# done

# apply "git@gitlab.com:dataalliance/products/brandaid/infra/k8s-infra.git"

apply "git@gitlab.com:dataalliance/products/data-onboarding/infra/data-ingest/k8s-infra.git"
apply "git@gitlab.com:dataalliance/products/ada-data/infra/composer/k8s-infra.git"
apply "git@gitlab.com:dataalliance/products/reporting-center/infra/frontend/k8s-infra.git"